﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InimeseTegemineConsole
{
    enum Sugu { Naine, Mees}
    class Inimene
    {
        public static List<Inimene> Inimesed = new List<Inimene>();

        public string Nimi;
        public Sugu Sugu;
        public Inimene Isa;
        public Inimene Ema;
        public List<Inimene> Lapsed = new List<Inimene>();
        public Inimene Kaasa;

        public Inimene() => Inimesed.Add(this); 

        public bool Abielu(Inimene kaasa)
        {
            if (Kaasa == null && kaasa.Kaasa == null && Sugu != kaasa.Sugu)
            {
                Kaasa = kaasa; kaasa.Kaasa = this;
                return true;
            }
            else return false;
        }

        public bool Lahutus()
        {
            if (Kaasa == null) return false;
            Kaasa.Kaasa = null;
            Kaasa = null;
            return true;

        }

        public Inimene TeeLaps(string nimi, Sugu sugu)
        {
            Inimene uus = new Inimene { Nimi = nimi, Sugu = sugu };
            LisaLaps(uus);
            Kaasa?.LisaLaps(uus);
            return uus;    
        }
        #region lahendus
        public Inimene TeeLaps(string nimi, Sugu sugu, Inimene partner)
        {
            Inimene uus = new Inimene { Nimi = nimi, Sugu = sugu };
            LisaLaps(uus);
            // valisin variandi, kus samasooline teine vanem jäetakse märkimata
            if (partner?.Sugu != Sugu) partner?.LisaLaps(uus);
            return uus;
        }
        #endregion
        public void LisaLaps(Inimene laps)
        {
            if (Sugu == Sugu.Mees) laps.Isa = this; else laps.Ema = this;
            Lapsed.Add(laps);
        }

        public override string ToString()
        {
            return $"{Sugu} {Nimi}" + (Isa == null && Ema == null ? ""
                : $" ({(Isa == null ? "" : "isa:"+Isa.Nimi)} {(Ema == null ? "" : "ema:" + Ema.Nimi)})");
                ;
        }

    }
}
