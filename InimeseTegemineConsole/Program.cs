﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InimeseTegemineConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene Henn = new Inimene { Nimi = "Henn", Sugu = Sugu.Mees };
            Henn.Abielu(new Inimene { Nimi = "Eve", Sugu = Sugu.Naine });
            Henn.TeeLaps("Krister", Sugu.Mees);
            Henn.TeeLaps("Holger", Sugu.Mees);
            Henn.Lahutus();
            Henn.Abielu(new Inimene { Nimi = "Maris", Sugu = Sugu.Naine });
            var ml = Henn.TeeLaps("Mai-Liis", Sugu.Naine);
            Henn.TeeLaps("Pille-Riin", Sugu.Naine);
            ml.Abielu(new Inimene { Nimi = "Leino", Sugu = Sugu.Mees });
            ml.TeeLaps("Triinu", Sugu.Naine);
            ml.TeeLaps("Tormi", Sugu.Mees);
            ml.TeeLaps("Greete", Sugu.Naine);
            foreach (var x in Inimene.Inimesed) Console.WriteLine(x);

            Henn.TeeLaps("Miku", Sugu.Mees, null);
            Henn.TeeLaps("Manni", Sugu.Naine, new Inimene() { Nimi = "Manni ema", Sugu = Sugu.Naine });
            Console.WriteLine("\nTestime uut funktsiooni\n");
            foreach (var x in Inimene.Inimesed) Console.WriteLine(x);

        }
    }
}
